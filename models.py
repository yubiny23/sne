# coding: utf-8
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()



class DBAREA(db.Model):
    __tablename__ = 'DB_AREA'

    ID_AREA = db.Column(db.Integer, primary_key=True)
    AREA = db.Column(db.String(100), nullable=False)
    CREATED = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())



class DBDOCUMENTTYPE(db.Model):
    __tablename__ = 'DB_DOCUMENT_TYPE'

    ID_DOCUMENT_TYPE = db.Column(db.Integer, primary_key=True)
    VARCHAR_50_ = db.Column(db.String(50), nullable=False)



class DBEMPLOYEE(db.Model):
    __tablename__ = 'DB_EMPLOYEE'

    ID_EMPLOYEE = db.Column(db.Integer, primary_key=True)
    NAME = db.Column(db.String(250), nullable=False)
    LASTNAME = db.Column(db.String(250), nullable=False)
    ID_SUBAREA = db.Column(db.ForeignKey('DB_SUBAREA.ID_SUBAREA', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False, index=True)
    ID_DOCUMENT_TYPE = db.Column(db.ForeignKey('DB_DOCUMENT_TYPE.ID_DOCUMENT_TYPE', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False, index=True)
    DOCUMENT = db.Column(db.String(50), nullable=False)
    CREATED = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    DB_DOCUMENT_TYPE = db.relationship('DBDOCUMENTTYPE', primaryjoin='DBEMPLOYEE.ID_DOCUMENT_TYPE == DBDOCUMENTTYPE.ID_DOCUMENT_TYPE', backref='dbemployees')
    DB_SUBAREA = db.relationship('DBSUBAREA', primaryjoin='DBEMPLOYEE.ID_SUBAREA == DBSUBAREA.ID_SUBAREA', backref='dbemployees')



class DBSUBAREA(db.Model):
    __tablename__ = 'DB_SUBAREA'

    ID_SUBAREA = db.Column(db.Integer, primary_key=True)
    SUBAREA = db.Column(db.String(100), nullable=False)
    ID_AREA = db.Column(db.ForeignKey('DB_AREA.ID_AREA', ondelete='RESTRICT', onupdate='RESTRICT'), nullable=False, index=True)
    CREATED = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    DB_AREA = db.relationship('DBAREA', primaryjoin='DBSUBAREA.ID_AREA == DBAREA.ID_AREA', backref='dbsubareas')
