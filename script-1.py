from flask import Flask, render_template, flash, request, redirect, url_for
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy

from models import db
from models import DBAREA , DBSUBAREA , DBDOCUMENTTYPE , DBEMPLOYEE


#create the object of Flask
app  = Flask(__name__)

app.config['SECRET_KEY'] = '916f4c31aaa35d6b867dae9a7f54270d'


#SqlAlchemy Database Configuration With Mysql
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://sne:123456@localhost:3306/sne'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)



#creating our routes
@app.route('/')
def index():
    
    return jsonify(
                    {"message": "Root", "severity": "info"}
                )



#area routes

@app.route('/areas/<id>' , methods = ['GET'])
def area_get(id):
    if request.method == 'GET':
        u = DBAREA.query.filter_by(ID_AREA=id).first()
        #u = db.session.query(DBAREA).filter(ID_AREA=id).one()
        db.session.commit()
        return jsonify(
                        {
                            "area": u.AREA ,
                            "id" : u.ID_AREA, 
                            "severity": "info"
                        }
                    )

@app.route('/areas' , methods = ['POST'])
def area_create():
    if request.method == 'POST':

        gArea = request.form.get('area')    
        u = DBAREA(AREA=gArea)
        db.session.add(u)
        db.session.commit()
        return jsonify(
                        {"message": "Root", "severity": "info"}
                    )

@app.route('/areas/<id>' , methods = ['DELETE'])
def area_delete(id):
    if request.method == 'DELETE':
        DBAREA.query.filter_by(ID_AREA=id).delete()
        db.session.commit()
        return jsonify(
                        {"message": "Eliminado", "severity": "info"}
                    )

@app.route('/areas/<id>' , methods = ['PATCH'])
def area_edit(id):
    if request.method == 'PATCH':
        gArea = request.form.get('area')
        x = db.session.query(DBAREA).get(id)
        x.AREA = gArea
        db.session.commit()
        return jsonify(
                        {"message": "Eliminado", "severity": "info"}
                    )





#run flask app
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=82)