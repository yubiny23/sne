/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     07/05/2021 01:45:22 p.m.                     */
/*==============================================================*/


drop table if exists DB_AREA;

drop table if exists DB_DOCUMENT_TYPE;

drop table if exists DB_EMPLOYEE;

drop table if exists DB_SUBAREA;

/*==============================================================*/
/* Table: DB_AREA                                               */
/*==============================================================*/
create table DB_AREA
(
   ID_AREA              int not null auto_increment,
   AREA                 varchar(100) not null,
   CREATED              datetime not null default CURRENT_TIMESTAMP,
   primary key (ID_AREA)
);

/*==============================================================*/
/* Table: DB_DOCUMENT_TYPE                                      */
/*==============================================================*/
create table DB_DOCUMENT_TYPE
(
   ID_DOCUMENT_TYPE     int not null auto_increment,
   VARCHAR_50_          varchar(50) not null,
   primary key (ID_DOCUMENT_TYPE)
);

/*==============================================================*/
/* Table: DB_EMPLOYEE                                           */
/*==============================================================*/
create table DB_EMPLOYEE
(
   ID_EMPLOYEE          int not null auto_increment,
   NAME                 varchar(250) not null,
   LASTNAME             varchar(250) not null,
   ID_SUBAREA           int not null,
   ID_DOCUMENT_TYPE     int not null,
   DOCUMENT             varchar(50) not null,
   CREATED              datetime not null default CURRENT_TIMESTAMP,
   primary key (ID_EMPLOYEE)
);

/*==============================================================*/
/* Table: DB_SUBAREA                                            */
/*==============================================================*/
create table DB_SUBAREA
(
   ID_SUBAREA           int not null auto_increment,
   SUBAREA              varchar(100) not null,
   ID_AREA              int not null,
   CREATED              datetime not null default CURRENT_TIMESTAMP,
   primary key (ID_SUBAREA)
);

alter table DB_EMPLOYEE add constraint FK_REFERENCE_1 foreign key (ID_SUBAREA)
      references DB_SUBAREA (ID_SUBAREA) on delete restrict on update restrict;

alter table DB_EMPLOYEE add constraint FK_REFERENCE_3 foreign key (ID_DOCUMENT_TYPE)
      references DB_DOCUMENT_TYPE (ID_DOCUMENT_TYPE) on delete restrict on update restrict;

alter table DB_SUBAREA add constraint FK_REFERENCE_2 foreign key (ID_AREA)
      references DB_AREA (ID_AREA) on delete restrict on update restrict;

